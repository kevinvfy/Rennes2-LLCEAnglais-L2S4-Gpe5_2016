Installation
------------

**ATTENTION : Cette procédure n'a pas l'air de fonctionner pour l'instant ! Elle n'est pas dangereuse, mais ne fonctionne pas (au moins sous Linux) - On ne peut que récupérer les cours, pas en envoyer**

- Allez sur le site de SparkleShare et installez-le (http://sparkleshare.org/)
- Démarrez SparkleShare, vous aurez un assistant

**Quand l'assistant proposera une clé RSA**


Ça sert à dire au site que l'ordinateur est digne de confiance (vu qu'on demande pas de mot de passe, ça le remplace, en disant "Cet ordi appartient à telle personne"


- Cliquez sur Copy. Allez ensuite sur GitLab (ici), 
et dans la colonne de gauche cliquez sur "Back to dashboard" et sur Profile Settings.
- Dans Profile Settings, cliquez sur "SSH keys",puis sur "Add SSH Key"
- Dans le champ Key, collez (clic droit - coller) la clé, et donnez lui un nom (par ex mon ordi)
- Cliquez sur "Add Key"
- Retourner ensuite dans SparkleShare

**Ajouter le projet**
- Faire un clic droit dur l'icone SparkleShare, dans la zone de notification (à coté de l'horloge pour Windows et Mac)
- Faire Clic droit - SparkleShare - Add hosted project

*Ecran Where is your project hosted*
- Cliquez sur "On my own server"
- Dans Adress, mettez *git@git.framasoft.org*
- Dans Remote Path, mettez */kevinvfy/Rennes2-LLCEAnglais-L2S4-Gpe5_2016*
- Normalement il ne devrait pas y avoir d'erreur, tout est bon !

Utilisation
-----------
**Avec SparkleShare**

Vous aurez un dossier SparkleShare sur votre ordinateur, qui se synchronisera tout seul avec
cet espace. Mettez tout simplement vos cours dans le bon dossier, en indiquant clairement de quoi parle le cours

Utilisez si possible des formats ouverts (format ODT de LibreOffice/OpenOffice) 
pour que tout le monde puisse les ouvrir même sans avoir Word ou Pages (Word sait faire des fichiers ODT depuis plusieurs versions)
Si possible, proposez aussi un fichier PDF, facile à ouvrir.

Si vous n'avez pas ni OpenOffice ni LibreOffice, vous pouvez télécharger la dernière version de LibreOffice ici :
https://fr.libreoffice.org/download/libreoffice-stable/

PS : je préfère LibreOffice à OpenOffice car OpenOffice est en cours d'abandon : les mises à jour sont rares, et de plus, même
si c'est loin d'être parfait (d'où mon conseil d'utiliser les formats ODT),
LibreOffice gère mieux les formats Microsoft Office (Word, Excel, etc)

**Directement sur Git**

Vous pouvez accéder aux cours en allant sur *git.framasoft.org*, en vous connectant. Choisissez le projet *Rennes2-LLCEAnglais-L2S4-Gpe5_2016*
Cliquez sur la colonne de gauche, sur Files. Cliquez sur le cours qui vous intéresse, puis sur Download.

Et voilà, c'est environ tout :D